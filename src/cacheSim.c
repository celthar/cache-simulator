#include "Cache.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGTH 16

// Eine Zeile zeichenweise aus der übergebenen Datei lesen
char* fgetline( FILE* file ) {
    int     index = 0;
    char    readChar;
    char*   readLine = (char*) malloc(MAX_LINE_LENGTH * sizeof(char));

    do {
        readChar = fgetc(file);

        if(readChar == EOF){
            free(readLine);

            return NULL;
        } else {
            readLine[index] = readChar;
        }

        index++;
    } while( (readChar != '\n') && (readChar != EOF) && (index < MAX_LINE_LENGTH) );

    return readLine;
}

int main( int argc, char** argv ){
    if(argc != 5){
        printf("Usage: cacheSim <cache_size> <cache_type> <cache_use> <in_file>\n");
        return EXIT_FAILURE;
    }

    int cache_size = atoi(argv[1]);         // Größe des Cache (in Bytes)
    int cache_type = atoi(argv[2]);         // Assoziativität des Cachee
    char cache_use = argv[3][0];            // Art des Cache (Data, Instruction, Unified)
    FILE* in_file  = fopen(argv[4], "r");   // Trace-File die eingelesen wird
    char* line;                             // Eingelesene Zeile
    int split_0;                            // Art des Zugriffs (Read/Write Data, Instruction Fetch)
    int split_1;                            // Addresse auf die Zugegriffen wird

    Cache* cache = newCache(cache_use, cache_size, cache_type);

    if( (cache != NULL) && (in_file != NULL) ){
        resetCache(cache);
        printCacheInformation(cache);

        while( (line = fgetline(in_file)) != NULL ){
            split_0 = strtol(strtok(line, " "), NULL, 10);
            split_1 = strtol(strtok(NULL, "\n"), NULL, 16);

            if( (cache->type == 'I') && (split_0 == 2) ){
                cycleCache(cache, split_1);
            } else if( (cache->type == 'D') && (split_0 == 0 || split_0 == 1 )){
                cycleCache(cache, split_1);
            } else if( cache->type == 'U' ){
                cycleCache(cache, split_1);
            }

            free(line);
        }

        printCacheContent(cache);
        printCacheInformation(cache);

        delCache(cache);
        fclose(in_file);
    }

    return EXIT_SUCCESS;
}

