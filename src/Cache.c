#include "Cache.h"

#include <stdio.h>
#include <stdlib.h>

#include <math.h>

#define FALSE 0
#define TRUE  1

Cache* newCache( char type, int size, int lines_per_set ){
    int cnt_sets;
    int cnt_lines;
    int lines = size / CACHE_LINE_SIZE;
    int sets = lines / lines_per_set;

    if( (lines % lines_per_set) != 0 ){
        printf("Wrong Alignment!\n");

        return NULL;
    }

    // Cache mit allen Werten initialisieren
    Cache* cache            = (Cache*) malloc(sizeof(Cache));
    cache->type             = type;
    cache->size             = size;
    cache->sets             = sets;
    cache->lines_per_set    = lines_per_set;
    cache->line_offset_bits = log2(CACHE_LINE_SIZE);
    cache->line_index_bits  = log2(sets);
    cache->line_tag_bits    = ADDRESS_WIDTH - cache->line_offset_bits - cache->line_index_bits;

    cache->hits             = 0.0;
    cache->miss             = 0.0;

    // Speicherplatz für die Cache-Sets allozieren
    cache->set              = (int***) malloc(sets * sizeof(int**));

    for( cnt_sets = 0; cnt_sets < sets; ++cnt_sets ){
        // Speicherplatz für die Cache-Entries eines Sets allozieren
        cache->set[cnt_sets]       = (int**) malloc(lines_per_set * sizeof(int*));

        for( cnt_lines = 0; cnt_lines < lines_per_set; ++cnt_lines ){
            // Speicherplatz für eine Cache-Line allozieren
            cache->set[cnt_sets][cnt_lines] = (int*) malloc(ENTRY_DEPTH * sizeof(int));
        }
    }

    return cache;
}

void delCache( Cache* cache ){
    int cnt_sets;
    int cnt_lines;

    for( cnt_sets = 0; cnt_sets < cache->sets; ++cnt_sets ){
        for( cnt_lines = 0; cnt_lines < cache->lines_per_set; ++cnt_lines ){
            free(cache->set[cnt_sets][cnt_lines]);
        }

        free(cache->set[cnt_sets]);
    }

    free(cache->set);
    free(cache);
}

void resetCache( Cache* cache ){
    int cnt_sets;
    int cnt_lines;
    int cnt_entries;

    cache->hits = 0.0;
    cache->miss = 0.0;

    for( cnt_sets = 0; cnt_sets < cache->sets; ++cnt_sets ){
        for( cnt_lines = 0; cnt_lines < cache->lines_per_set; ++cnt_lines ){
            for( cnt_entries = 0; cnt_entries < ENTRY_DEPTH; ++cnt_entries ){
                cache->set[cnt_sets][cnt_lines][cnt_entries] = 0;
            }
        }
    }
}

void cycleCache( Cache* cache, int address ){
    int cnt_lines;
    int hit = FALSE;
    int hit_index = 0;
    int lru_index = 0;

    // Offset, Index und Tag der Addresse berechnen
    int line_offset = address % CACHE_LINE_SIZE;
    address         = address >> cache->line_offset_bits;
    int line_index  = address % cache->sets;
    address         = address >> cache->line_index_bits;
    int line_tag    = address;

    // Im Set (durch Index berechnet) nach dem Tag suchen
    for( cnt_lines = 0; cnt_lines < cache->lines_per_set; ++cnt_lines ){
        if( cache->set[line_index][cnt_lines][INDEX_TAG] == line_tag ){
            hit       = TRUE;
            hit_index = cnt_lines;
        }
    }

    // Set altern lassen
    ageLine(cache, line_index);

    // Wenn hit, dann Anzahl der Hits erhöhen und Alter des Eintrags
    // auf 0 setzen. Ansonsten Anzahl der Misses erhöhen, ältesten
    // Eintrag bestimmen und ersetzen
    if( hit ){
        ++(cache->hits);
        cache->set[line_index][hit_index][INDEX_AGE] = 0;
    } else {
        ++(cache->miss);
        lru_index = getLRU(cache, line_index);
        cache->set[line_index][lru_index][INDEX_AGE] = 0;
        cache->set[line_index][lru_index][INDEX_TAG] = line_tag;
    }
}

void ageLine( Cache* cache, int line_index ){
    int cnt_lines;

    for( cnt_lines = 0; cnt_lines < cache->lines_per_set; ++cnt_lines ){
        ++(cache->set[line_index][cnt_lines][INDEX_AGE]);
    }
}

int getLRU( Cache* cache, int line_index ){
    int cnt_lines;
    int maxFoundAge = 0;
    int oldestIndex = 0;

    for( cnt_lines = 0; cnt_lines < cache->lines_per_set; ++cnt_lines ){
        if( cache->set[line_index][cnt_lines][INDEX_AGE] > maxFoundAge ){
            maxFoundAge = cache->set[line_index][cnt_lines][INDEX_AGE];
            oldestIndex = cnt_lines;
        }
    }

    return oldestIndex;
}

void printCacheInformation( Cache* cache ){
    printf("Cache-Information\n");
    printf("\tType              : %c\n", cache->type);
    printf("\tSize              : %d Byte\n", cache->size);
    printf("\tSets              : %d\n", cache->sets);
    printf("\tLines per Set     : %d\n", cache->lines_per_set);
    printf("\n");
    printf("\tHits              : %f\n", cache->hits);
    printf("\tMisses            : %f\n", cache->miss);
    printf("\tHitRatio          : %f\n", getHitRatio(cache));
    printf("\n");
    printf("\tLine-Offset-Bits  : %d Bits\n", cache->line_offset_bits);
    printf("\tLine-Index-Bits   : %d Bits\n", cache->line_index_bits);
    printf("\tLine-Tag-Bits     : %d Bits\n", cache->line_tag_bits);
}

void printCacheContent( Cache* cache ){
    int cnt_sets;
    int cnt_lines;

    printf("Cache-Content\n");

    for( cnt_sets = 0; cnt_sets < cache->sets; ++cnt_sets ){
        printf("Set %d\n", cnt_sets);

        for( cnt_lines = 0; cnt_lines < cache->lines_per_set; ++cnt_lines ){
            printf("\tEntry %d: %d, %d\n",
                cnt_lines,
                cache->set[cnt_sets][cnt_lines][INDEX_AGE],
                cache->set[cnt_sets][cnt_lines][INDEX_TAG]);
        }
    }
}

double getHitRatio( Cache* cache ){
    if( cache->miss == 0.0 ) {
        return 0.0;
    } else {
        return (cache->hits / (cache->hits + cache->miss));
    }
}

