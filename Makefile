CC 	= gcc
CFLAGS 	= -Iinc -Llib -Wall -lm

.PHONY: clean

main:
	$(CC) $(CFLAGS) -o bin/cacheSim src/Cache.c src/cacheSim.c

clean:
	rm bin/cacheSim

