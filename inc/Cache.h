#ifndef __CACHE_H__
#define __CACHE_H__

#define CACHE_LINE_SIZE  4
#define ADDRESS_WIDTH   32
#define ENTRY_DEPTH      2
#define INDEX_AGE        0
#define INDEX_TAG        1

/*
 *  Struktur, welche einen Cache darstellt (ohne Datenspeicherung)
 *  sowie einige wichtige Informationen zum Cache speichert.
 */
typedef struct{
    char    type;               // Typ des Caches
                                // I = Instruction-Cache
                                // D = Data-Cache
                                // U = Unified-Cache
    int     size;               // Größe des Cache
    int     sets;               // Anzahl der Sets im Cache
    int     lines_per_set;      // Anzahl der Cache-Lines pro Set
    int     line_offset_bits;   // Anzahl der Bits für den Cache-Offset
    int     line_index_bits;    // Anzahl der Bits für den Cache-Index
    int     line_tag_bits;      // Anzahl der Bits für den Cache-Tag

    double  hits;               // Anzahl der Hits
    double  miss;               // Anzahl der Misses

    int***  set;                // 3-dimensionales Array für die Cache-Daten
                                // (speichert hier die Tags und das Alter)
} Cache;

/*
 *  Erstellt einen neuen Cache vom Typ <type> mit <sets> Sets und
 *  <lines_per_set> Cache-Lines pro Set.
 *  Gibt NULL zurück, falls das Alignment nicht stimmt.
 */
Cache* newCache( char type, int sets, int lines_per_set );

/*
 *  Löscht den Cache <cache>.
 */
void delCache( Cache* cache );

/*
 *  Setzt alle Cache-Tags und Cache-Lines des Cache <cache> auf 0.
 */
void resetCache( Cache* cache );

/*
 *  Sucht die Addresse <address> in Cache <cache>.
 */
void cycleCache( Cache* cache, int address );

/*
 *  Lässt alle Einträge im Set <line_index> um eine Einheit altern.
 */
void ageLine( Cache* cache, int line_index );

/*
 *  Gibt den 'least-recently-used'-Eintrag aus dem Set <line_index> heraus.
 */
int getLRU( Cache* cache, int line_index );

/*
 *  Gibt Informationen zu Cache <cache> aus.
 */
void printCacheInformation( Cache* cache );

/*
 *  Gibt die Inhalte des Cache <cache> aus.
 */
void printCacheContent( Cache* cache );

/*
 *  Gibt das Verhältnis von Hits zu Misses zurück.
 */
double getHitRatio( Cache* cache );

#endif /* __CACHE_H__ */

